.. sphinx_test documentation master file, created by
   sphinx-quickstart on Mon Feb 20 14:50:15 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx_test's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart/index.rst
   package/index.rst
   examples/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
