# -*- coding: utf-8 -*-

from distutils.core import setup

setup(name='sphinx_test',
      version='0.1',
      py_modules=['module_one'],
      packages=['package_one'],
      scripts=['scripts/main.py']
      )
