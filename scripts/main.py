#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

import package_one
import module_one


def main():
    print package_one.module_two.function_two()
    print module_one.function_one()
    time.sleep(2)

if __name__ == '__main__':
    main()