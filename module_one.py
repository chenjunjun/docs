# -*- coding: utf-8 -*-


def function_one(arg1=1, arg2=2):
    """函数简介

    函数说明

    :param arg1: 参数1
    :type arg1: int
    :param arg2: 参数2
    :type arg2: str
    :return: 返回值的说明
    :rtype: str
    :raise RuntimeError: 可能会跑出的异常
    """
    return "function_one"


class ClassOne(object):

    """ 这里是对类的说明 """

    def __init__(self, arg1=1, arg2=2):
        """构造函数

        构造函数的说明

        :param arg1: arg1的说明
        :type arg1: int
        :param arg2: arg2 的说明
        :type arg2: int
        :return: 返回值的说明
        :rtype: ClassOne
        """
        pass

    def open(self):
        """打开

        :return: 返回自己
        :rtype: ClassOne
        """
        return self

    def close(self):
        """ 关闭

        :return: 没有返回
        """
        pass