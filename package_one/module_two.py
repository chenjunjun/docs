# -*- coding: utf-8 -*-


def function_two(arg1=1, arg2=2):
    """函数简介

    函数说明

    :param arg1: 参数1
    :type arg1: int
    :param arg2: 参数2
    :type arg2: str
    :return: 返回值
    :rtype: str
    """
    return 'function_two'


class ClassTwo(object):

    """ Class Two 简介 """

    def __init__(self, arg1, arg2=2):
        """构造函数

        :param arg1: 参数1
        :type arg1: int
        :param arg2: 参数2
        :type arg2: str
        :return: 返回值说明
        :rtype: ClassTwo
        """
        pass