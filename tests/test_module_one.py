# -*- coding: utf-8 -*-

import unittest

import module_one


class FunctionOneTest(unittest.TestCase):
    def test(self):
        self.assertEqual('function_one', module_one.function_one())


class ClassOneTest(unittest.TestCase):
    def setUp(self):
        self.class_one = module_one.ClassOne()

    def test(self):
        pass


if __name__ == '__main__':
    unittest.main()

